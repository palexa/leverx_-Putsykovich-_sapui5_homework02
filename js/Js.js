
var arr;
var id=0;


OrderInfo = {
    id			: "",
    createdAt	: "",
    customer	: "",
    status		: "",
    shippedAt	: ""
};

Product = {
    name		: "",
    id			: "",
    price		: "",
    currency	: "",
    quantity	: "",
    totalPrice	: ""
};


ShipTo = {
    name	: "",
    street	: "",
    ZIP		: "",
    city	: "",
    Region	: "",
    Country	: ""
};

CustomerInfo = {
    firstName	: "",
    lastName	: "",
    address		: "",
    phone		: "",
    email		: "",
    photo		: ""
};

Order = {
    id			: "",
    OrderInfo	: {},
    ShipTo		: {},
    CustomerInfo: {},
    products	: []
};
var Orders = [
    {
        id: "1",
        OrderInfo: {
            createdAt	: "10.08.1991",
            customer	: "Alfreds Futterkiste",
            status		: "Accepted",
            shippedAt	: "8.09.1991"
        },
        ShipTo: {
            name: "Maria Anders",
            Address: "Obere Str. 57",
            ZIP: "12209",
            Region: "Germany",
            Country: "Germany"
        },
        CustomerInfo: {
            firstName: "Maria",
            lastName: "Anders",
            address: "Obere Str. 57",
            phone: "030-0074321",
            email: "Maria.Anders@company.com"
        },
        products: [
            {
                id			: "1",
                name		: "Chai",
                price		: "18",
                currency	: "EUR",
                quantity	: "2",
                totalPrice	: "36"
            },
            {
                id			: "2",
                name		: "Aniseed Syrup",
                price		: "10",
                currency	: "USD",
                quantity	: "3",
                totalPrice	: "30"
            },
            {
                id			: "3",
                name		: "Chef Anton's Cajun Seasoning",
                price		: "22",
                currency	: "USD",
                quantity	: "2",
                totalPrice	: "44"
            },
            {
                id			: "4",
                name		: "Chef Anton's Gumbo Mix",
                price		: "36",
                currency	: "EUR",
                quantity	: "21",
                totalPrice	: "756"
            },
            {
                id			: "5",
                name		: "Grandma's Boysenberry Spread",
                price		: "25",
                currency	: "USD",
                quantity	: "5",
                totalPrice	: "125"
            }
        ]
    },
    {
        id: "2",
        OrderInfo: {
            createdAt	: "23.12.2006",
            customer	: "Bon app",
            status		: "Pending",
            shippedAt	: "13.02.2007"
        },
        ShipTo: {
            name: "Laurence Lebihan",
            Address: "12, rue des Bouchers",
            ZIP: "13008",
            Region: "France",
            Country: "France"
        },
        CustomerInfo: {
            firstName: "Laurence",
            lastName: "Lebihan",
            address: "12, rue des Bouchers",
            phone: "91.24.45.40",
            email: "Laurence.Lebihan@company.com"
        },
        products: [
            {
                id			: "1",
                name		: "Queso Cabrales",
                price		: "21",
                currency	: "EUR",
                quantity	: "5",
                totalPrice	: "105"
            },
            {
                id			: "2",
                name		: "Queso Manchego La Pastora",
                price		: "38",
                currency	: "EUR",
                quantity	: "3",
                totalPrice	: "114"
            },
            {
                id			: "3",
                name		: "Pavlova",
                price		: "120",
                currency	: "RUB",
                quantity	: "5",
                totalPrice	: "600"
            },
            {
                id			: "4",
                name		: "Sir Rodney's Marmalade",
                price		: "5",
                currency	: "BYN",
                quantity	: "3",
                totalPrice	: "15"
            },
            {
                id			: "5",
                name		: "Genen Shouyu",
                price		: "40",
                currency	: "USD",
                quantity	: "7",
                totalPrice	: "280"
            },
            {
                id			: "6",
                name		: "Tofu",
                price		: "23.25",
                currency	: "USD",
                quantity	: "1",
                totalPrice	: "23.25"
            },
            {
                id			: "7",
                name		: "Alice Mutton",
                price		: "32",
                currency	: "UAH",
                quantity	: "39",
                totalPrice	: "1248"
            }
        ]
    }
];
function isEmpty(str) {
    if (str.trim() == '')
        return true;

    return false;
}
function createList(value) {
    //var list_container = document.getElementById("list-container");
    var ul = document.getElementById("ul");

        var li = document.createElement("li");
        var order_num = document.createElement("div");
        var order_date = document.createElement("div");
        var num = document.createElement("h4");
        var customer = document.createElement("h5");
        var shipped = document.createElement("h5");
        var ordered = document.createElement("h3");
        var status = document.createElement("h5");
        li.id = value;
        num.innerText = "Order "+value;
        customer.innerText = Orders[value]["OrderInfo"].customer;
        shipped.innerText = "Shipped:" + Orders[value]["OrderInfo"].shippedAt;
        ordered.innerText = Orders[value]["OrderInfo"].createdAt;
        status.innerText = Orders[value]["OrderInfo"].status;
        order_num.className = "Order__num";
        order_date.className = "Order__date";
        //list_container.appendChild(ul);
        ul.appendChild(li);
        li.appendChild(order_num);
        li.appendChild(order_date);
        order_num.appendChild(num);
        order_num.appendChild(customer);
        order_num.appendChild(shipped);
        order_date.appendChild(ordered);
        order_date.appendChild(status);


    arr = document.getElementsByTagName("li");
    for (var i = 0; i < arr.length; i++) {
        arr[i].addEventListener("click", function (event) {
            id=this.id;
            Address_meanings.innerHTML="";
            for (var key in Orders[0]["ShipTo"]) {
                Address_meanings.innerHTML += Orders[id]["ShipTo"][key] + "<br>";
            }
            Order_information();
        });

    }
}
function allOrders() {
    for (var i=0;i<Orders.length;i++){
        createList(i);
    }

}
function Order_information() {
var Order_inform=document.querySelector("#Order_Inform");
Order_inform.innerText="";
var Order_num=document.createElement("h3");
var Customer=document.createElement("h5");
var Ordered=document.createElement("h5");
var Shipped=document.createElement("h5");
Order_num.innerText="Order "+id;
Customer.innerText="Customer "+Orders[id].OrderInfo.customer;
Ordered.innerText="Ordered "+Orders[id].OrderInfo.createdAt;
Shipped.innerText="Shipped "+Orders[id].OrderInfo.shippedAt;
Order_inform.appendChild(Order_num);
Order_inform.appendChild(Customer);
Order_inform.appendChild(Ordered);
Order_inform.appendChild(Shipped);
Order_inform.innerHTML+="<input type=\"image\" src=\"../img/car.png\" width=\"12%\" id=\"car\">\n" +
    "                <input type=\"image\" src=\"../img/man.png\" width=\"12%\">";
    car.addEventListener("click", function (event) {
        if (isEmpty(Address_meanings.innerHTML)) {
            for (var key in Orders[id]["ShipTo"]) {
                Address_meanings.innerHTML += Orders[id]["ShipTo"][key] + "<br>";
            }
        }
    });
}
function myFunc() {
    //Order.innerHTML="<script>alert( 1 );</scr" + "ipt>";
    var Address_meanings = document.querySelector("#Address_meanings");
    var car = document.querySelector("#car");

    Address_meanings.addEventListener("click", function (event) {
        for (var key in Orders[0]["ShipTo"]) {
            Address_meanings.innerHTML += Orders[0]["ShipTo"][key] + "<br>";
        }
    });
    allOrders();
 Searching();
 //loadDoc();
    getURL("http://localhost:3000/api/Orders/2/products",function1)

}

function Searching(){
    var ul = document.getElementById("ul");
    var searchInput=document.getElementById("searchInput");

    searchInput.addEventListener("change", function(event) {
        var value = event.target.value.toLowerCase();
        ul.innerHTML="";
        for(i=0;i<Orders.length; i++){
            var obj =  Orders[i]["OrderInfo"];
            var propertyArray=Object.values(obj);
            for(j=0; j<propertyArray.length; j++){
                var searchContainer= propertyArray[j].toLowerCase();
                var orderSearchId = Orders[i]["id"];
                var res = search(value, searchContainer);
                if(res === 1){
                    createList(orderSearchId-1);
                }
            }

        }

    });
}


function search(re, str) {
    if (str.search(re) != -1) {
        return 1;
    } else {
        return 0;
    }
}

var modalWindow = {
    _block: null,
    _win: null,
    initBlock: function() {
        _block = document.getElementById('blockscreen'); //Получаем наш блокирующий фон по ID

        //Если он не определен, то создадим его
        if (!_block) {
            var parent = document.getElementsByTagName('body')[0]; //Получим первый элемент тега body
            var obj = parent.firstChild; //Для того, чтобы вставить наш блокирующий фон в самое начало тега body
            _block = document.createElement('div'); //Создаем элемент div
            _block.id = 'blockscreen'; //Присваиваем ему наш ID
            parent.insertBefore(_block, obj); //Вставляем в начало
            _block.onclick = function() { modalWindow.close(); } //Добавим обработчик события по нажатию на блокирующий экран - закрыть модальное окно.
        }
        _block.style.display = 'inline'; //Установим CSS-свойство
    },
    initWin: function(width, html) {
        _win = document.getElementById('modalwindow'); //Получаем наше диалоговое окно по ID
        //Если оно не определено, то также создадим его по аналогии
        if (!_win) {
            var parent = document.getElementsByTagName('body')[0];
            var obj = parent.firstChild;
            _win = document.createElement('div');
            _win.id = 'modalwindow';
            _win.style.padding = '0 0 5px 0';
            parent.insertBefore(_win, obj);
        }
        _win.style.width = width + 'px'; //Установим ширину окна
        _win.style.display = 'inline'; //Зададим CSS-свойство

        _win.innerHTML = '<div  class="search">'+
            '<input type="search" name="" placeholder="поиск" class="input" id="searchInput" />'+
            '<input type="submit" name="" value="" class="submit" id="submit" onclick="modalWindow.close()" />'+

            '</div>'; //Добавим нужный HTML-текст в наше диалоговое окно

        //Установим позицию по центру экрана

        _win.style.left = '50%'; //Позиция по горизонтали
        _win.style.top = '50%'; //Позиция по вертикали

        //Выравнивание по центру путем задания отрицательных отступов
        _win.style.marginTop = -(_win.offsetHeight / 2) + 'px';
        _win.style.marginLeft = -(width / 2) + 'px';
    }, close: function() {
        document.getElementById('blockscreen').style.display = 'none';
        document.getElementById('modalwindow').style.display = 'none';
    }, show: function(width, html) {
        modalWindow.initBlock();
        modalWindow.initWin(width, html);
    }
}//the end
function loadDoc() {
    var xhr=new XMLHttpRequest();
    if(this.readyState == 4 && this.status == 200){
       var result=this.responseText;
    };
    xhr.open("GET","http://localhost:3000/api/OrderProducts",true);
    xhr.send();

}
function getURL(url,callback){
    var xhr =new XMLHttpRequest();

    xhr.onreadystatechange=function () {
        if(this.status<400)
            callback(JSON.parse(this.responseText));
        else
            callback(null,new Error("Request fail:"+this.statusText));
    };
    xhr.open("GET",url,true);
    xhr.send(null);
}
function function1(data,error) {

    console.dir(data);

}